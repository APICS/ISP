
/*
	MIT License
	
	Copyright (c) 2018 Quantum Feather
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;

// TODO: Auto-generated Javadoc
/**
 * The first cutscene explains the world and introduces our characters.
 */
public class Act1 extends BasicGameState {

	/** The Constant ID. */
	public static final int ID = Application.ACT1;

	/** The background image. */
	Image bg;

	/** The midground image. */
	Image mg;

	/** The foreground image. */
	Image fg;

	/** The brother spritesheet. */
	SpriteSheet brother;

	/** The blob spritesheet. */
	SpriteSheet blob;

	/** The brother 2 spritesheet. */
	SpriteSheet brother2;

	/** The blob walk spritesheet. */
	SpriteSheet blobWalk;

	/** The brother idle image array. */
	Image[] brotherIdle;

	/** The blob idle image array. */
	Image[] blobIdle;

	/** The brother run image array. */
	Image[] brotherRun;

	/** The blob left walk image array. */
	Image[] blobLeftWalk;

	/** The current sprite. */
	int currentSprite;

	/** The current sprite 2. */
	int currentSprite2;

	/** The time. */
	int time;

	/**
	 * start <br>
	 * Set to true when you want the dialog to start
	 */
	boolean start;

	/** The text font. */
	AngelCodeFont textFont;

	/** The main font. */
	AngelCodeFont mainFont;

	/** The current speaker. */
	String currentSpeaker;

	/** The current line. */
	String currentLine;

	/**
	 * The maximum chars per line
	 */
	int maxLen;

	/**
	 * The Y coordinate of speaker
	 */
	int startY;

	/**
	 * The current line of script
	 */
	int currentLn;

	/**
	 * The current character being displayed. Used for animation
	 */
	int[] x;

	/**
	 * Stores every speaker
	 */
	List<String> speaker = new ArrayList<String>();

	/**
	 * Stores every line
	 */
	List<String> line = new ArrayList<String>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#keyReleased(int, char)
	 */
	@Override
	public void keyReleased(int key, char c) {
		currentLn++;
		Arrays.fill(x, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		bg = new Image("assets/bgs/sky.png", false, Image.FILTER_NEAREST).getScaledCopy((float) 1000 / 137);
		mg = new Image("assets/bgs/arena.png", false, Image.FILTER_NEAREST).getScaledCopy((float) 1000 / 137);
		fg = new Image("assets/bgs/swords.png", false, Image.FILTER_NEAREST).getScaledCopy((float) 1000 / 137);

		currentSprite = 0;
		time = 0;

		blobLeftWalk = new Image[8];
		blobIdle = new Image[6];
		brotherIdle = new Image[5];
		brotherRun = new Image[6];

		blob = new SpriteSheet(new Image("assets/blob/idle.png"), 34, 34, 46);
		brother = new SpriteSheet(new Image("assets/brother/brother.png"), 84, 84, 0);
		brother2 = new SpriteSheet(new Image("assets/brother/brotherRun.png"), 79, 84, 14);
		blobWalk = new SpriteSheet(new Image("assets/blob/walk.png"), 34, 34, 46);
		for (int x = 0; x < 5; x++) {
			brotherRun[x] = brother2.getSprite(x, 0);
			brotherRun[x].setFilter(Image.FILTER_NEAREST);
			brotherRun[x] = brotherRun[x].getScaledCopy((float) 1.62);
		}

		for (int x = 0; x < 6; x++) {
			blobIdle[x] = blob.getSprite(x, 0);
			blobIdle[x].setFilter(Image.FILTER_NEAREST);
			blobIdle[x] = blobIdle[x].getScaledCopy(4);
		}

		for (int x = 0; x < 4; x++) {
			brotherIdle[x] = brother.getSprite(x, 0);
			brotherIdle[x].setFilter(Image.FILTER_NEAREST);
			brotherIdle[x] = brotherIdle[x].getScaledCopy((float) 1.62);
		}
		for (int x = 0; x < 8; x++) {
			blobLeftWalk[x] = blobWalk.getSprite(x, 0);
			blobLeftWalk[x].setFilter(Image.FILTER_NEAREST);
			blobLeftWalk[x] = blobLeftWalk[x].getScaledCopy(4);
		}

		start = true;

		// textFont = 18px
		textFont = new AngelCodeFont("assets/fonts/textFont.fnt", new Image("assets/fonts/textFont.png"));
		// mainFont = 32px
		mainFont = new AngelCodeFont("assets/fonts/mainFont.fnt", new Image("assets/fonts/mainFont.png"));
		// Set max characters per line
		maxLen = 70;
		// Number of lines for animation
		x = new int[3];
		// Beginning y coordinate
		startY = 20;

		// Add script data to ArrayLists
		try (BufferedReader br = new BufferedReader(new FileReader("assets/script1.txt"))) {
			int x = 0;
			for (String ln; (ln = br.readLine()) != null;) {
				if (x % 2 == 0)
					speaker.add(ln);
				else
					line.add(ln);
				x++;
			}
			speaker.add(" ");
			line.add(" ");
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}

		currentLine = line.get(0);
		currentSpeaker = speaker.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.drawImage(bg, 0, 0);
		g.drawImage(mg, 0, 0);
		g.drawImage(fg, 0, 0);
		if (time < 250) {
			g.drawImage(blobLeftWalk[currentSprite], 1000 - time * 2, 600 - 132);
			g.drawImage(brotherRun[currentSprite], -200 + time * 2, 600 - 132);
		} else {
			g.drawImage(blobIdle[currentSprite], 500, 600 - 132);
			g.drawImage(brotherIdle[currentSprite2], 300, 600 - 132);
		}

		// Hides dialog box once end of script is reached
		if (currentLn < speaker.size() - 1 && start) {
			// Draw dialog box
			g.setAntiAlias(true);
			g.setColor(Color.white);
			g.fill(new RoundedRectangle(10, startY - 10, 980, 150 - startY, 11));

			// Display current speaker
			g.setColor(Color.black);
			g.setFont(mainFont);
			g.drawString(currentSpeaker, 20, startY);

			// Display first line
			g.setColor(Color.black);
			g.setFont(textFont);
			g.drawString(currentLine.substring(0, x[0]), 22, startY + 50);

			// If the string is longer than one line
			if (currentLine.length() > maxLen) {
				// Find the last space on the first line
				int last = currentLine.substring(0, maxLen).lastIndexOf(" ") + 1;
				// Display the second line
				g.drawString(currentLine.substring(last, last + x[1]), 22, startY + 80);

				// If the string is longer than two lines
				if (currentLine.length() > maxLen * 2) {
					// Find the last space on the second line
					int last2 = last + currentLine.substring(last, maxLen * 2).lastIndexOf(" ") + 1;
					// Display the third line
					g.drawString(currentLine.substring(last2, last2 + x[2]), 22, startY + 110);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int arg2) throws SlickException {
		time++;
		if (start) {
			// String needs 1 line
			if (currentLine.length() <= maxLen) {
				// If the first line isn't full, add one more char
				if (x[0] < currentLine.length())
					x[0]++;

				// String needs more than 1 line
			} else {
				// If the first line isn't full, add one more char
				if (x[0] < currentLine.substring(0, maxLen).lastIndexOf(" "))
					x[0]++;

				// String needs 3 lines
				if (currentLine.length() >= maxLen * 2) {
					// If the first line is full & the second line isn't full, add one more char
					if (x[0] == currentLine.substring(0, maxLen).lastIndexOf(" ")
							&& x[1] < currentLine.substring(x[0], maxLen * 2).lastIndexOf(" "))
						x[1]++;
					// If the second line is full & the third line isn't full, add one more char
					else if (x[1] == currentLine
							.substring(currentLine.substring(0, maxLen).lastIndexOf(" "), maxLen * 2).lastIndexOf(" ")
							&& x[2] < currentLine.substring(x[0] + x[1]).length() - 1 && x[2] < maxLen)
						x[2]++;

					// String needs 2 lines
				} else if (currentLine.length() > maxLen && currentLine.length() < maxLen * 2) {
					// If the first line is full & the second line isn't full, add one more char
					if (x[0] == currentLine.substring(0, maxLen).lastIndexOf(" ")
							&& x[1] < currentLine.substring(x[0]).length() - 1)
						x[1]++;
				}
			}

			// Prevents IndexOutOfBoundsException & displays blank lines
			if (currentLn < speaker.size()) {
				currentLine = line.get(currentLn);
				currentSpeaker = speaker.get(currentLn);
			}
		}

		if (time % 6 == 0) {
			currentSprite2++;
		}
		if (time % 16 == 0) {
			currentSprite++;
		}
		if (currentSprite == 5) {
			currentSprite = 0;
		}
		if (currentSprite2 == 4) {
			currentSprite2 = 0;
		}

		if (currentLn == speaker.size() / 2 - 1) {
			Vars.currentLevel=0;
            Vars.money=100;
            Vars.kills=0;
			sbg.getState(6).init(gc, sbg);
			sbg.enterState(6, new EmptyTransition(), new EmptyTransition());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	@Override
	public int getID() {
		return Act1.ID;
	}
}