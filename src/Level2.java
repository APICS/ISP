
/*
	MIT License
	
	Copyright (c) 2018 Quantum Feather
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;

// TODO: Auto-generated Javadoc
/**
 * The Class Level2.
 */
public class Level2 extends BasicGameState {

	/** The Constant ID. */
	// ID we return to class 'Application'
	public static final int ID = Application.LEVEL2;

	/** The Constant BLOB_SCALE. */
	public static final float BLOB_SCALE = 4;

	/** The Constant MAP_FLOOR. */
	public static final int MAP_FLOOR = 600;

	/** The Constant GOBLIN_WIDTH. */
	public static final int GOBLIN_WIDTH = 102;

	/** The Constant BLOB_HEIGHT. */
	public static final int BLOB_HEIGHT = 34;

	/** The Constant BLOB_WIDTH. */
	public static final int BLOB_WIDTH = 34;

	/** The bg. */
	Image bg;

	/** The mg. */
	Image mg;

	/** The fg. */
	Image fg;

	/** The player hitbox. */
	public Shape playerHitbox;

	/** The enemy hitbox. */
	public Shape[] enemyHitbox = new Shape[20];

	/** The punch hitbox left. */
	public Shape punchHitboxLeft;

	/** The punch hitbox right. */
	public Shape punchHitboxRight;

	/** The background coordinate. */
	int backgroundCoordinate;

	/** The background speed. */
	int backgroundSpeed = 2;

	/** The blob speed. */
	int blobSpeed = 2;

	/** The background coordinate L. */
	int backgroundCoordinateL = -1000;

	/** The background coordinate C. */
	int backgroundCoordinateC = 0;

	/** The background coordinate R. */
	int backgroundCoordinateR = 1000;

	/** The blob hp. */
	int blobHp = 100;

	/** The level. */
	int level = 2;

	/** The blob damage. */
	int blobDamage;

	/** The win. */
	boolean win = false;

	/** The kills. */
	int kills = 0;

	/** The is dead. */
	boolean isDead = false;

	/** The choice. */
	int choice = 0;

	/** The goblin hp. */
	int[] goblinHp = new int[20];

	/** The blob immunity. */
	int blobImmunity = 0;

	/** The enemy immunity. */
	int[] enemyImmunity = new int[20];

	/** The enemy X. */
	int[] enemyX = new int[20];

	/** The enemy Y. */
	int enemyY[] = new int[20];

	/** The slime sprite. */
	int slimeSprite = 0;

	/** The blob X. */
	static int blobX = (1000 - BLOB_WIDTH * 4) / 2;

	/** The blob Y. */
	static int blobY = (int) (MAP_FLOOR - BLOB_HEIGHT * BLOB_SCALE);

	/** The last pos X. */
	int lastPosX;

	/** The last pos Y. */
	int lastPosY;

	/** The time. */
	float time;

	/** The current sprite. */
	int currentSprite = 0;

	/** The blob punch. */
	SpriteSheet blobPunch;

	/** The blob walk. */
	SpriteSheet blobWalk;

	/** The blob idle. */
	SpriteSheet blobIdle;

	/** The slime walk. */
	SpriteSheet slimeWalk;

	/** The is punching. */
	int isPunching = -1;

	/** The idle sprite. */
	int idleSprite = 0;

	/** The time since punch. */
	int timeSincePunch = 0;

	/** The vertical speed. */
	float verticalSpeed = 0.0f;

	/** The is jumping. */
	boolean isJumping = false;

	/** The text font. */
	AngelCodeFont titleFont, mainFont, subtitleFont, textFont;

	/** The blob left walk. */
	Image[] blobLeftWalk;

	/** The blob right walk. */
	Image[] blobRightWalk;

	/** The blob left idle. */
	Image[] blobLeftIdle;

	/** The blob right idle. */
	Image[] blobRightIdle;

	/** The blob left punch. */
	Image[] blobLeftPunch;

	/** The blob right punch. */
	Image[] blobRightPunch;

	/** The goblin left walk. */
	Image[] goblinLeftWalk;

	/** The goblin right walk. */
	Image[] goblinRightWalk;

	/** The death screen. */
	Image deathScreen;

	/** The goblin dead. */
	Image goblinDead;

	/** The enemy is jumping. */
	boolean[] enemyIsJumping;

	/** The enemy vertical speed. */
	float[] enemyVerticalSpeed;

	/** The hp cross. */
	Image hpCross;

	/** The punch sound. */
	Sound punchSound;

	/** The blob direction. */
	boolean blobDirection = false; // false is left, true is right

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame)
	 */
	// init-method for initializing all resources
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

		// Scale the blob's damage based off of how close they are to recommended food
		// group values and nutrient values
		blobDamage = (int) ((50 - Math.abs(Vars.recommendedDairy - Vars.blobDairy) * 3
				- Math.abs(Vars.recommendedFruits - Vars.blobFruits) * 3
				- Math.abs(Vars.recommendedMeat - Vars.blobMeat) * 3
				- Math.abs(Vars.recommendedGrains - Vars.blobGrains) * 3
				- Math.abs(Vars.recommendedSodium - Vars.blobSodium) / 1000
				- Math.abs(Vars.recommendedPotassium - Vars.blobPotassium) / 1000
				- Math.abs(Vars.recommendedProtein - Vars.blobProtein) / 10
				- Math.abs(Vars.recommendedFat - Vars.blobFat) / 10)) / (Vars.difficulty + 1);
		// Set minimum blob damage
		if (blobDamage <= 20 / (Vars.difficulty + 1))
			blobDamage = 20 / (Vars.difficulty + 1);
		// Scale the blob's health based off of how close they are to recommended food
		// group values and nutrient values
		blobHp = (int) ((200 - Math.abs(Vars.recommendedDairy - Vars.blobDairy) * 5
				- Math.abs(Vars.recommendedFruits - Vars.blobFruits) * 5
				- Math.abs(Vars.recommendedMeat - Vars.blobMeat) * 5
				- Math.abs(Vars.recommendedGrains - Vars.blobGrains) * 5
				- Math.abs(Vars.recommendedSodium - Vars.blobSodium) / 1000
				- Math.abs(Vars.recommendedPotassium - Vars.blobPotassium) / 1000
				- Math.abs(Vars.recommendedProtein - Vars.blobProtein) / 10
				- Math.abs(Vars.recommendedFat - Vars.blobFat) / 10)) / (Vars.difficulty + 1);
		// Set minimum blob health
		if (blobHp <= 50 / (Vars.difficulty + 1))
			blobHp = 50 / (Vars.difficulty + 1);

		// Arrays to hold each enemy's current place and action
		enemyY = new int[20];
		enemyHitbox = new Shape[20];
		enemyIsJumping = new boolean[20];
		enemyVerticalSpeed = new float[20];
		goblinHp = new int[20];
		enemyX = new int[20];

		// Setting level start conditions
		backgroundSpeed = 2;
		blobSpeed = 2;
		backgroundCoordinateL = -1000;
		backgroundCoordinateC = 0;
		backgroundCoordinateR = 1000;
		level = 2;
		win = false;
		kills = 0;
		isDead = false;
		choice = 0;
		isDead = false;
		choice = 0;

		// Holds blob immunity after taking damage
		blobImmunity = 0;
		// Holds the current sprite for the enemy
		slimeSprite = 0;

		// Start coordinates of blob on screen
		blobX = (1000 - BLOB_WIDTH * 4) / 2;
		blobY = (int) (MAP_FLOOR - BLOB_HEIGHT * BLOB_SCALE);

		// Setting initial conditions for blob
		isPunching = -1;
		idleSprite = 0;
		timeSincePunch = 0;
		verticalSpeed = 0.0f;
		isJumping = false;

		// The punch sound
		punchSound = new Sound("assets/music/punch.ogg");

		// Spawning each enemy onto the screen
		for (int x = 0; x < 20; x++) {
			goblinHp[x] = 100;
			enemyX[x] = -8000 + 800 * x;
			enemyImmunity[x] = 0;
			enemyY[x] = 480;
			enemyIsJumping[x] = false;
			enemyVerticalSpeed[x] = 0.0f;
		}

		// Standard font sizes: 8,9,10,11,12,14,18,24,30,36,48,60,72,96
		// titleFont = 96px
		titleFont = new AngelCodeFont("assets/fonts/titleFont.fnt", new Image("assets/fonts/titleFont.png"));
		// mainFont = 32px
		mainFont = new AngelCodeFont("assets/fonts/mainFont.fnt", new Image("assets/fonts/mainFont.png"));
		// subtitleFont = 48px
		subtitleFont = new AngelCodeFont("assets/fonts/subtitleFont.fnt", new Image("assets/fonts/subtitleFont.png"));
		// textFont = 18px
		textFont = new AngelCodeFont("assets/fonts/textFont.fnt", new Image("assets/fonts/textFont.png"));

		// Create spritesheet for different animations
		blobWalk = new SpriteSheet(new Image("assets/blob/walk.png"), BLOB_WIDTH, BLOB_HEIGHT, 46);
		blobIdle = new SpriteSheet(new Image("assets/blob/idle.png"), BLOB_WIDTH, BLOB_HEIGHT, 46);
		blobPunch = new SpriteSheet(new Image("assets/blob/punch.png"), 80, BLOB_HEIGHT, 0);
		slimeWalk = new SpriteSheet(new Image("assets/slime/slime.png"), 32, 32, 0);

		// Load assets
		goblinDead = new Image("assets/goblin/goblindead.png", false, Image.FILTER_NEAREST);
		hpCross = new Image("assets/Health/HPcross.png", false, Image.FILTER_NEAREST);

		bg = new Image("assets/bgs/sky.png", false, Image.FILTER_NEAREST);
		mg = new Image("assets/bgs/castle.png", false, Image.FILTER_NEAREST);
		fg = new Image("assets/bgs/castle.png", false, Image.FILTER_NEAREST);

		deathScreen = new Image("assets/bgs/deathscreen.png", false, Image.FILTER_NEAREST);

		// Set size of each animation array according to number of sprites
		blobLeftWalk = new Image[8];
		blobRightWalk = new Image[8];
		blobLeftPunch = new Image[8];
		blobRightPunch = new Image[8];
		blobLeftIdle = new Image[6];
		blobRightIdle = new Image[6];
		goblinLeftWalk = new Image[4];
		goblinRightWalk = new Image[4];

		// Place each animation into its respective array
		for (int x = 0; x < blobWalk.getHorizontalCount(); x++) {
			blobLeftWalk[x] = blobWalk.getSprite(x, 0);
			blobLeftWalk[x].setFilter(Image.FILTER_NEAREST);
			blobLeftWalk[x] = blobLeftWalk[x].getScaledCopy(BLOB_SCALE);

			blobRightWalk[x] = blobWalk.getSprite(x, 0).getFlippedCopy(true, false);
			blobRightWalk[x].setFilter(Image.FILTER_NEAREST);
			blobRightWalk[x] = blobRightWalk[x].getScaledCopy(BLOB_SCALE);

		}

		for (int x = 0; x < blobIdle.getHorizontalCount(); x++) {
			blobLeftIdle[x] = blobIdle.getSprite(x, 0);
			blobLeftIdle[x].setFilter(Image.FILTER_NEAREST);
			blobLeftIdle[x] = blobLeftIdle[x].getScaledCopy(BLOB_SCALE);

			blobRightIdle[x] = blobIdle.getSprite(x, 0).getFlippedCopy(true, false);
			blobRightIdle[x].setFilter(Image.FILTER_NEAREST);
			blobRightIdle[x] = blobRightIdle[x].getScaledCopy(BLOB_SCALE);
		}

		for (int x = 0; x < blobPunch.getHorizontalCount(); x++) {
			blobLeftPunch[x] = blobPunch.getSprite(x, 0);
			blobLeftPunch[x].setFilter(Image.FILTER_NEAREST);
			blobLeftPunch[x] = blobLeftPunch[x].getScaledCopy(BLOB_SCALE);

			blobRightPunch[x] = blobPunch.getSprite(x, 0).getFlippedCopy(true, false);
			blobRightPunch[x].setFilter(Image.FILTER_NEAREST);
			blobRightPunch[x] = blobRightPunch[x].getScaledCopy(BLOB_SCALE);
		}

		for (int x = 0; x < slimeWalk.getHorizontalCount(); x++) {
			goblinLeftWalk[x] = slimeWalk.getSprite(x, 0).getFlippedCopy(true, false);
			goblinLeftWalk[x].setFilter(Image.FILTER_NEAREST);
			goblinLeftWalk[x] = goblinLeftWalk[x].getScaledCopy(BLOB_SCALE);

			goblinRightWalk[x] = slimeWalk.getSprite(x, 0);
			goblinRightWalk[x].setFilter(Image.FILTER_NEAREST);
			goblinRightWalk[x] = goblinRightWalk[x].getScaledCopy(BLOB_SCALE);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	// render-method for all the things happening on-screen
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {

		// Draw the background onto the screen
		g.drawImage(bg.getScaledCopy((float) 1000 / 137), backgroundCoordinateL, 0);
		g.drawImage(bg.getScaledCopy((float) 1000 / 137), backgroundCoordinateR, 0);
		g.drawImage(bg.getScaledCopy((float) 1000 / 137), backgroundCoordinateC, 0);
		g.drawImage(mg.getScaledCopy((float) 1000 / 137), backgroundCoordinateL, 0);
		g.drawImage(mg.getScaledCopy((float) 1000 / 137), backgroundCoordinateR, 0);
		g.drawImage(mg.getScaledCopy((float) 1000 / 137), backgroundCoordinateC, 0);
		g.drawImage(fg.getScaledCopy((float) 1000 / 137), backgroundCoordinateL, 0);
		g.drawImage(fg.getScaledCopy((float) 1000 / 137), backgroundCoordinateR, 0);
		g.drawImage(fg.getScaledCopy((float) 1000 / 137), backgroundCoordinateC, 0);

		// Draw the enemies
		for (int x = 0; x < enemyX.length; x++) {
			if (enemyX[x] < blobX) {
				g.drawImage(goblinLeftWalk[slimeSprite], enemyX[x], enemyY[x]);
			} else {
				g.drawImage(goblinRightWalk[slimeSprite], enemyX[x], enemyY[x]);
			}
		}

		// Draw the blob health bar
		g.drawImage(hpCross, 0, 550);
		g.setColor(Color.black);
		g.setFont(mainFont);
		if (blobHp >= 100)
			g.drawString("" + blobHp, 14, 584);
		else if (blobHp < 100 && blobHp >= 10)
			g.drawString("" + blobHp, 25, 584);
		else
			g.drawString("" + blobHp, 38, 584);

		// Draw the blob walking
		if (isPunching == -1) {
			if (blobDirection == false) {
				g.drawImage(blobLeftWalk[currentSprite], blobX, blobY);
			} else {
				g.drawImage(blobRightWalk[currentSprite], blobX + blobSpeed, blobY);
			}
		}

		// Draw the blob punching
		if (isPunching != -1) {
			if (blobDirection == false) {
				g.drawImage(blobLeftPunch[isPunching / 3], blobX - 48, blobY);
			} else {
				g.drawImage(blobRightPunch[isPunching / 3], blobX - 48, blobY);
			}
		}

		// Lose screen
		if (isDead == true) {
			g.drawImage(deathScreen.getScaledCopy((float) 1000 / 137), 0, 0);
			g.setColor(Color.black);
			g.setFont(titleFont);
			g.drawString("UNCONSCIOUS", 0, 150);
			g.setFont(mainFont);
			g.drawString("Continue?", 400, 275);
			g.drawString("Yes", 400, 325);
			g.drawString("No", 550, 325);
		}

		// Win screen
		if (win == true) {
			g.drawImage(deathScreen.getScaledCopy((float) 1000 / 137), 0, 0);
			g.setColor(Color.black);
			g.setFont(titleFont);
			g.drawString("BEAT LEVEL " + level, 0, 150);
			g.setFont(mainFont);
			g.drawString("Continue?", 400, 275);
			g.drawString("Yes", 400, 325);
			g.drawString("No", 550, 325);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mouseReleased(int, int, int)
	 */
	// Mouse detection
	@Override
	public void mouseReleased(int button, int x, int y) {
		if (button == 0) {
			if (x >= 400 && x <= 480 && y >= 325 && y <= 355) {
				choice = 1;
			} else if (x >= 550 && x <= 604 && y >= 325 && y <= 355) {
				choice = 2;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, int)
	 */
	// update-method with all the magic happening in it
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int arg2) throws SlickException {
		Input input = gc.getInput();// Needed for user input

		// Creating infinite scrolling background
		if (backgroundCoordinateL >= 0) {
			backgroundCoordinateC = 0;
			backgroundCoordinateL = -1000;
			backgroundCoordinateR = 1000;
		}

		if (backgroundCoordinateR <= 0) {
			backgroundCoordinateC = 0;
			backgroundCoordinateR = 1000;
			backgroundCoordinateL = -1000;
		}

		// Actual movement of the blob logic
		if (blobX < 200 - BLOB_WIDTH * BLOB_SCALE) {
			if (input.isKeyDown(Input.KEY_LEFT)) {
				blobSpeed = 0;
			}

			if (input.isKeyDown(Input.KEY_RIGHT)) {
				blobSpeed = 2;
			}
		} else if (blobX > 780) {
			if (input.isKeyDown(Input.KEY_LEFT)) {
				blobSpeed = 0;
			}

			if (input.isKeyDown(Input.KEY_LEFT)) {
				blobSpeed = 2;
			}
		}

		// Incrementing some time variables
		time++;
		blobImmunity++;
		for (int x = 0; x < enemyX.length; x++) {
			enemyImmunity[x]++;
		}

		// Creating the blob's hitbox
		playerHitbox = new Rectangle(blobX + 30, blobY + 10, 25 * BLOB_SCALE, 85);
		// Limiting the punch rate of the blob
		if (timeSincePunch > 0)
			timeSincePunch++;
		if (timeSincePunch > 68)
			timeSincePunch = 0;

		// Creating punching blob's hitbox
		if (isPunching != -1 && blobDirection == false && timeSincePunch < 68) {
			playerHitbox = new Rectangle(blobX + 50, blobY, 26 * BLOB_SCALE, 85);
		}

		// Left and right punch hitboxes
		punchHitboxLeft = new Rectangle(blobX - 15, blobY + 50, 80, 20);
		punchHitboxRight = new Rectangle(blobX + 114, blobY + 50, 120, 20);

		// Checking if punch hits any enemies and that the enemy is not immune
		for (int x = 0; x < enemyX.length; x++) {
			enemyHitbox[x] = new Rectangle(enemyX[x] + 5, enemyY[x], 110, 120);

			if (punchHitboxLeft.intersects(enemyHitbox[x]) && isPunching != -1 && blobDirection == false
					&& enemyImmunity[x] > 30 && enemyIsJumping[x] == false
					|| punchHitboxRight.intersects(enemyHitbox[x]) && isPunching != -1 && blobDirection == true
							&& enemyImmunity[x] > 30 && enemyIsJumping[x] == false) {
				punchSound.play();
				goblinHp[x] -= blobDamage;
				enemyImmunity[x] = 0;
				if (goblinHp[x] <= 0) {
					kills++;
					Vars.kills++;
					Vars.money += 30;
				}

			}
			// Checking if enemies hit the blob
			if (enemyHitbox[x].intersects(playerHitbox) && blobImmunity > 30 && enemyIsJumping[x] == false) {
				blobHp -= 10;
				blobImmunity = 0;
			}
		}

		// Takes input for jump
		if (input.isKeyDown(Input.KEY_UP) && isJumping == false) {
			isJumping = true;
			verticalSpeed = -7.0f;
		}
		if (isJumping == true) {
			verticalSpeed += 0.15f;
		}

		// Takes input for punch
		if (input.isKeyDown(Input.KEY_X) && isPunching == -1 && isJumping == false) {
			isPunching = 0;
			if (blobDirection == false)
				timeSincePunch = 1;
		}

		if (isPunching != -1) {
			isPunching++;
		}

		// Movement code for blob moving left
		if (isPunching == -1) {
			if (input.isKeyDown(Input.KEY_LEFT)) {
				blobDirection = false;
				blobX -= blobSpeed;
				backgroundCoordinateL += backgroundSpeed;
				backgroundCoordinateC += backgroundSpeed;
				backgroundCoordinateR += backgroundSpeed;
				for (int x = 0; x < enemyX.length; x++) {
					enemyX[x] += 2;
				}
				if (time % (6) == 0) {
					if (currentSprite == 7) {
						currentSprite = 0;
					} else {
						currentSprite++;
					}
				}
			}

			// Movement code for blob moving right
			else if (input.isKeyDown(Input.KEY_RIGHT)) {
				blobDirection = true;
				blobX += blobSpeed;
				backgroundCoordinateL -= backgroundSpeed;
				backgroundCoordinateC -= backgroundSpeed;
				backgroundCoordinateR -= backgroundSpeed;
				for (int x = 0; x < enemyX.length; x++) {

					enemyX[x] -= 2;

				}
				if (time % (6) == 0) {
					if (currentSprite == 7) {
						currentSprite = 0;
					} else {
						currentSprite++;
					}
				}
			}

		}

		// Enemy AI
		for (int x = 0; x < enemyX.length; x++) {
			if (lastPosX != blobX) {
				if (enemyX[x] < blobX && blobX > lastPosX) {
					enemyX[x] -= 2;
				} else if (enemyX[x] + GOBLIN_WIDTH / 2 < blobX && blobX < lastPosX) {
					enemyX[x] += 2;
				}
				if (enemyX[x] > blobX && blobX < lastPosX) {
					enemyX[x] += 2;
				} else if (enemyX[x] > blobX && blobX > lastPosX) {
					enemyX[x] -= 2;
				}

				lastPosX = blobX;
				lastPosY = blobY;
			}
		}

		// Allow blob to punch again
		if (isPunching == 23) {
			isPunching = -1;
		}

		// Add vertical speed to the blob's Y coordinate
		blobY += verticalSpeed;

		// Set the floor of the map
		if (blobY > MAP_FLOOR - BLOB_HEIGHT * BLOB_SCALE) {
			blobY = MAP_FLOOR - BLOB_HEIGHT * (int) BLOB_SCALE;
			verticalSpeed = 0;
			isJumping = false;
		}

		// Right invisible wall
		if (blobX > 780) {
			blobX = 780;
		}

		// Left invisible wall
		if (blobX < -105)
			blobX = -105;

		// Move enemies towards the blob
		for (int x = 0; x < enemyX.length; x++) {
			if (enemyX[x] < blobX)
				enemyX[x]++;

			if (enemyX[x] > blobX)
				enemyX[x]--;

			// Sets death of enemy
			if (goblinHp[x] <= 0 && enemyIsJumping[x] == false) {
				enemyIsJumping[x] = true;
				enemyVerticalSpeed[x] = -5.0f;
			}
		}

		// Updating sprites
		if (time % (8) == 0) {
			if (slimeSprite == 3) {
				slimeSprite = 0;
			} else {
				slimeSprite++;
			}
		}

		// Lose condition
		if (blobHp <= 0) {
			isDead = true;
			kills = 0;
		}

		// Win condition

		if (kills == 2) {
			win = true;
			kills = 0;
		}

		// Increment level
		if (win == true && kills > 0) {
			level++;
		}

		// Menu choice after winning
		if (win == true) {
			if (choice == 1) {
				Vars.currentLevel++;
				sbg.getState(Application.STORE).init(gc, sbg);
				sbg.enterState(Application.STORE, new EmptyTransition(), new EmptyTransition());
			} else if (choice == 2) {
				MainMenu.buttonPressed = 0;
				sbg.getState(Application.MAINMENU).init(gc, sbg);
				sbg.enterState(Application.MAINMENU, new EmptyTransition(), new EmptyTransition());
			}
		}

		// Menu choice after losing
		if (isDead == true) {
			if (choice == 1) {
				sbg.getState(Application.LEVEL2).init(gc, sbg);
				sbg.enterState(Application.LEVEL2, new EmptyTransition(), new EmptyTransition());
			} else if (choice == 2) {
				MainMenu.buttonPressed = 0;
				sbg.getState(Application.MAINMENU).init(gc, sbg);
				sbg.enterState(Application.MAINMENU, new EmptyTransition(), new EmptyTransition());
			}
		}

		// Enemy animation while dying
		for (int x = 0; x < enemyX.length; x++) {
			if (enemyIsJumping[x] == true) {
				enemyVerticalSpeed[x] += 0.15f;
				enemyY[x] += enemyVerticalSpeed[x];
				if (enemyX[x] < blobX) {

					enemyX[x] -= 3;
				}

				else if (enemyX[x] > blobX) {

					enemyX[x] += 3;

				}
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	// Returning 'ID' from class 'MainMenu'
	@Override
	public int getID() {
		return Level2.ID;
	}
}
