
/*
	MIT License
	
	Copyright (c) 2018 Quantum Feather
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

// TODO: Auto-generated Javadoc
/**
 * The Class Vars.
 */
public class Vars extends BasicGameState {

	/** The Constant ID. */
	// ID we return to class 'Application'
	public static final int ID = 99;

	/** The Constant recommendedFruits. */
	public static final double recommendedFruits = 8;

	/** The Constant recommendedGrains. */
	public static final double recommendedGrains = 7;

	/** The Constant recommendedDairy. */
	public static final double recommendedDairy = 3.5;

	/** The Constant recommendedMeat. */
	public static final double recommendedMeat = 3;

	/** The Constant recommendedSodium. */
	public static final double recommendedSodium = 2300;

	/** The Constant recommendedPotassium. */
	public static final double recommendedPotassium = 4700;

	/** The Constant recommendedProtein. */
	public static final double recommendedProtein = 52;

	/** The Constant recommendedProtein2. */
	public static final double recommendedProtein2 = 150;

	/** The Constant recommendedFat. */
	public static final double recommendedFat = 100;

	/** The blob fruits. */
	public static double blobFruits;

	/** The blob grains. */
	public static double blobGrains;

	/** The blob dairy. */
	public static double blobDairy;

	/** The blob meat. */
	public static double blobMeat;

	/** The blob sodium. */
	public static int blobSodium;

	/** The blob potassium. */
	public static int blobPotassium;

	/** The blob protein. */
	public static int blobProtein;

	/** The blob fat. */
	public static int blobFat;

	/** The money. */
	public static int money = 100;

	/** The difficulty. */
	public static int difficulty = 0;

	/** The kills. */
	public static int kills = 0;

	/** The title font. */
	public static AngelCodeFont titleFont;

	/** The main font. */
	public static AngelCodeFont mainFont;

	/** The subtitle font. */
	public static AngelCodeFont subtitleFont;

	/** The text font. */
	public static AngelCodeFont textFont;

	/** The current level. */
	public static int currentLevel;

	/** The name. */
	public static String name;

	/** The scores. */
	public static String[][] scores = { { "Jim", "10" }, { "Tim", "9" }, { "John", "8" }, { "Tom", "7" },
			{ "Wendy", "6" }, { "Sam", "5" }, { "George", "4" }, { "Bill", "3" }, { "Jose", "2" }, { "Bob", "1" },
			{ ".", "." } };

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame)
	 */
	// init-method for initializing all resources
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		Vars.titleFont = new AngelCodeFont("assets/fonts/titleFont.fnt", new Image("assets/fonts/titleFont.png"));
		mainFont = new AngelCodeFont("assets/fonts/mainFont.fnt", new Image("assets/fonts/mainFont.png"));
		subtitleFont = new AngelCodeFont("assets/fonts/subtitleFont.fnt", new Image("assets/fonts/subtitleFont.png"));
		textFont = new AngelCodeFont("assets/fonts/textFont.fnt", new Image("assets/fonts/textFont.png"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	// render-method for all the things happening on-screen
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, int)
	 */
	// update-method with all the magic happening in it
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int arg2) throws SlickException {
		sbg.enterState(1, new FadeOutTransition(), new FadeInTransition());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	// Returning 'ID' from class 'MainMenu'
	@Override
	public int getID() {
		return Vars.ID;
	}
}