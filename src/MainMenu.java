import java.util.Arrays;
import java.util.Comparator;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

// TODO: Auto-generated Javadoc
/**
 * The Class MainMenu.
 */
public class MainMenu extends BasicGameState {

	/** The Constant ID. */
	// ID we return to class 'Application'
	public static final int ID = Application.MAINMENU;

	/** The background image. */
	Image bg;

	/** The midground image. */
	Image mg;

	/** The foreground image. */
	Image fg;

	/** The banner. */
	Image banner;

	/** The quit button. */
	Image quitButton;

	/** The credits. */
	Image credits;

	/** The back button. */
	Image back;

	/** The Kyle banner. */
	Image kyle;

	/** The Alim banner. */
	Image alim;

	/** The settings banner. */
	Image settingsBanner;

	/** The music banner. */
	Image musicBanner;

	/** The extra banner. */
	Image extraBanner;

	/** The fonts. */
	AngelCodeFont titleFont;

	/** The main font. */
	AngelCodeFont mainFont;

	/** The subtitle font. */
	AngelCodeFont subtitleFont;

	/** The text font. */
	AngelCodeFont textFont;

	/** The volume X. */
	int volumeX;

	/** The sound toggle. */
	boolean soundToggle;

	/** The set name. */
	boolean setName;

	/**
	 * The button pressed. <br>
	 * 0 = Main Menu <br>
	 * 1 = Play <br>
	 * 2 = Settings <br>
	 * 3 = Rules <br>
	 * 4 = Scores <br>
	 * 5 = Credits <br>
	 * 6 = Quit <br>
	 * 7 = Exit() <br>
	 */
	public static int buttonPressed;

	/**
	 * The settings. <br>
	 * 0 = Volume (0-100)<br>
	 * 1 = Music (0=On, 1=Off)<br>
	 * 2 = Difficulty (0=Easy,1=Medium,2=Hard)
	 */
	int[] settings;

	/** The music on/off options. */
	Color[] music = { Color.black, Color.decode("#595959") };

	/** The difficulty options. */
	Color[] difficulty = { Color.black, Color.decode("#595959"), Color.decode("#595959") };

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		// Load Images
		bg = new Image("assets/bgs/sky.png", false, Image.FILTER_NEAREST);
		mg = new Image("assets/bgs/arena.png", false, Image.FILTER_NEAREST);
		fg = new Image("assets/bgs/swords.png", false, Image.FILTER_NEAREST);
		quitButton = new Image("assets/banners/quit.png", false, Image.FILTER_NEAREST);
		banner = new Image("assets/banners/mainButtons.png", false, Image.FILTER_NEAREST);
		credits = new Image("assets/banners/credits.png", false, Image.FILTER_NEAREST);
		back = new Image("assets/banners/back.png", false, Image.FILTER_NEAREST);
		kyle = new Image("assets/banners/kyle.png", false, Image.FILTER_NEAREST);
		alim = new Image("assets/banners/alim.png", false, Image.FILTER_NEAREST);
		settingsBanner = new Image("assets/banners/settings.png", false, Image.FILTER_NEAREST);
		musicBanner = new Image("assets/banners/music.png", false, Image.FILTER_NEAREST);
		extraBanner = new Image("assets/banners/extraCredit.png", false, Image.FILTER_NEAREST);

		settings = new int[3];
		settings[0] = 50;

		// Standard font sizes: 8,9,10,11,12,14,18,24,30,36,48,60,72,96
		// titleFont = 96px
		titleFont = new AngelCodeFont("assets/fonts/titleFont.fnt", new Image("assets/fonts/titleFont.png"));
		// mainFont = 32px
		mainFont = new AngelCodeFont("assets/fonts/mainFont.fnt", new Image("assets/fonts/mainFont.png"));
		// subtitleFont = 48px
		subtitleFont = new AngelCodeFont("assets/fonts/subtitleFont.fnt", new Image("assets/fonts/subtitleFont.png"));
		// textFont = 18px
		textFont = new AngelCodeFont("assets/fonts/textFont.fnt", new Image("assets/fonts/textFont.png"));

		Vars.scores[10][0] = Vars.name;
		Vars.scores[10][1] = String.valueOf(Vars.kills);
		Arrays.sort(Vars.scores, new Comparator<String[]>() {
			@Override
			public int compare(final String[] first, final String[] second) {
				return Double.valueOf(second[1]).compareTo(Double.valueOf(first[1]));
			}
		});
		Vars.scores[10][0] = null;
		Vars.scores[10][1] = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		// Ratio to scale background assets
		final float scale = (float) 1000 / 137;

		// Backgrounds
		g.drawImage(bg.getScaledCopy(scale), 0, 0);
		g.drawImage(mg.getScaledCopy(scale), 0, 0);
		g.drawImage(fg.getScaledCopy(scale), 0, 0);

		// Draw Title
		g.setColor(Color.black);
		g.setFont(titleFont);
		g.drawString("Fisticuffs", 123, 0);
		if (buttonPressed >= 2 && buttonPressed <= 5) {
			// Back Button
			g.setColor(Color.black);
			g.drawImage(back.getScaledCopy(3), 10, 580);
			g.setFont(mainFont);
			g.drawString("Back", 72, 585);
		}

		if (buttonPressed == 0) {
			// Change font for menu items
			g.setFont(mainFont);

			// Banners
			int bannerX = 121;
			int bannerY = 140;
			int bannerDisp = 100;
			// Banner: 85px x 19px before scaling, 366px x 82px afterwards
			// Play Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX, bannerY);
			g.drawString("Play", bannerX + 130, bannerY + 13);
			// Settings Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX, bannerY + bannerDisp);
			g.drawString("Settings", bannerX + 90, bannerY + 113);
			// Rules Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX, bannerY + bannerDisp * 2);
			g.drawString("Rules", bannerX + 122, bannerY + 213);

			// Highscores Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX + 400, bannerY);
			g.drawString("Scores", bannerX + 505, bannerY + 13);
			// Credits Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX + 400, bannerY + bannerDisp);
			g.drawString("Credits", bannerX + 505, bannerY + 113);
			// Quit Banner
			g.drawImage(banner.getScaledCopy(4.3f), bannerX + 400, bannerY + bannerDisp * 2);
			g.drawString("Quit", bannerX + 540, bannerY + 213);

		} else if (buttonPressed == 2) {
			// 96px x 19px = 576px x 114px
			g.drawImage(settingsBanner.getScaledCopy(6), 212, 138);
			g.setFont(subtitleFont);
			g.drawString("Settings", 337, 153);

			// Settings Headers
			g.setFont(mainFont);
			g.drawString("Volume", 245, 270);
			g.drawString("Music", 280, 330);
			g.drawString("Difficulty", 180, 390);
			g.drawString("Highscores", 160, 450);

			// Volume Slider
			Rectangle rect = new Rectangle(450, 276, settings[0] * 3.3f, 22);
			g.setColor(Color.transparent);
			g.draw(rect);
			g.setColor(Color.decode("#3eba3e"));
			g.fill(rect);
			g.setColor(Color.black);
			g.setFont(textFont);
			g.drawString(settings[0] + "%", volumeX, 277);
			g.setColor(Color.black);
			g.drawRect(450, 275, 330, 23);

			// Music On/Off
			g.setFont(mainFont);
			g.setColor(music[0]);
			g.drawString("ON", 446, 330);
			g.setColor(music[1]);
			g.drawString("OFF", 558, 330);

			// Difficulty
			g.setFont(mainFont);
			g.setColor(difficulty[0]);
			g.drawString("Easy", 446, 390);
			g.setColor(difficulty[1]);
			g.drawString("Medium", 606, 390);
			g.setColor(difficulty[2]);
			g.drawString("Hard", 809, 390);

			// High Scores
			g.setFont(mainFont);
			g.setColor(Color.black);
			g.drawString("Clear", 446, 450);
			g.drawString("Set Name", 446, 490);
			g.drawString(Vars.name, 446, 530);

		} else if (buttonPressed == 5) {
			// 90px x 19px = 540px x 114px
			g.drawImage(credits.getScaledCopy(6), 230, 150);
			g.setFont(subtitleFont);
			g.drawString("Credits", 360, 165);

			// Kyle's Banner
			// 43px x 72px = 151px x 252px
			g.drawImage(extraBanner.getScaledCopy(3.5f), 50, 272);
			g.setFont(textFont);
			g.drawString("Beta", 100, 310);
			g.drawString("Tester", 86, 330);
			g.drawString("---------", 70, 350);
			g.drawString("Ian", 105, 370);
			g.drawString("Smyth", 86, 390);

			// Kyle's Banner
			// 43px x 72px = 151px x 252px
			g.drawImage(kyle.getScaledCopy(3.5f), 300, 272);
			g.setFont(textFont);
			g.drawString("Project", 330, 310);
			g.drawString("Lead", 348, 330);
			g.drawString("---------", 320, 350);
			g.drawString("Kyle", 348, 370);
			g.drawString("Schwartz", 320, 390);

			// Alim's Banner
			// 43px x 72px = 151px x 252px
			g.drawImage(alim.getScaledCopy(3.5f), 549, 272);
			g.drawString("Project", 579, 310);
			g.drawString("Member", 581, 330);
			g.drawString("---------", 569, 350);
			g.drawString("Alim", 597, 370);
			g.drawString("Zaib", 598, 390);

			// Music's Banner
			// 43px x 72px = 151px x 252px
			g.drawImage(musicBanner.getScaledCopy(3.5f), 799, 272);
			g.drawString("Music", 840, 310);
			g.drawString("Creator", 827, 330);
			g.drawString("---------", 819, 350);
			g.drawString("Ozzed", 841, 370);
			g.drawString("Ozzed.net", 820, 390);

		} else if (buttonPressed == 6) {
			// 76px x 28px = 608px x 224px
			int bannerX = 196;
			int bannerY = 150;

			g.setColor(Color.black);

			g.drawImage(quitButton.getScaledCopy(8), bannerX, bannerY);
			g.setFont(mainFont);
			g.drawString("Are you sure?", bannerX + 146, bannerY + 80);

			g.drawString("Yes", bannerX + 170, bannerY + 130);
			g.drawString("No", bannerX + 374, bannerY + 130);
		} else if (buttonPressed == 3) {
			// 90px x 19px = 540px x 114px
			g.drawImage(credits.getScaledCopy(6), 230, 150);
			g.setFont(subtitleFont);
			g.drawString("Rules", 395, 167);

			g.setAntiAlias(true);
			g.setColor(Color.white);
			g.fill(new RoundedRectangle(144, 276, 360, 110, 11));

			g.fill(new RoundedRectangle(534, 276, 170, 70, 11));

			g.setColor(Color.black);
			g.setFont(mainFont);
			g.drawString("Movement", 150, 280);

			g.setFont(textFont);
			g.drawString("Right Arrow Key: Move right", 150, 316);
			g.drawString("Left Arrow Key: Move left", 150, 336);
			g.drawString("Up Arrow Key: Jump", 150, 356);

			g.setColor(Color.black);
			g.setFont(mainFont);
			g.drawString("Attack", 540, 280);

			g.setFont(textFont);
			g.drawString("X: Punch", 540, 316);
		} else if (buttonPressed == 4) {
			// 96px x 19px = 576px x 114px
			g.drawImage(settingsBanner.getScaledCopy(6), 212, 138);
			g.setFont(subtitleFont);
			g.drawString("Scores", 364, 153);

			g.setFont(mainFont);
			for (int x = 0; x < 10; x++) {
				g.drawString(Vars.scores[x][0], 380, 250 + x * 30);
				g.drawString(Vars.scores[x][1], 580, 250 + x * 30);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int arg2) throws SlickException {
		if (buttonPressed == 1) { // Play Button Pressed

			sbg.getState(Application.ACT1).init(gc, sbg);
			sbg.enterState(Application.ACT1, new FadeOutTransition(), new FadeInTransition());
		} else if (buttonPressed == 7)
			gc.exit();

		if (settings[0] < 0)
			settings[0] = 0;
		else if (settings[0] > 100)
			settings[0] = 100;
		else if (settings[0] < 10)
			volumeX = 610;
		else if (settings[0] == 100)
			volumeX = 590;
		else
			volumeX = 600;

		if (settings[1] == 0) {
			music[0] = Color.black;
			music[1] = Color.decode("#595959");
			if (soundToggle) {
				BlackScreen.track1.resume();
				soundToggle = false;
			}
		} else {
			music[1] = Color.black;
			music[0] = Color.decode("#595959");
			BlackScreen.track1.pause();
			soundToggle = true;
		}

		Arrays.fill(difficulty, Color.decode("#595959"));
		if (settings[2] == 0)
			difficulty[0] = Color.black;
		else if (settings[2] == 1)
			difficulty[1] = Color.black;
		else if (settings[2] == 2)
			difficulty[2] = Color.black;

		BlackScreen.track1.setVolume((float) settings[0] / 100);

		Vars.difficulty = settings[2];

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#keyReleased(int, char)
	 */
	@Override
	public void keyReleased(int key, char c) {
		if (key == 28)
			setName = false;
		if (setName && Vars.name.length() < 10) {
			if (key == 14 && Vars.name.length() > 0)
				Vars.name = Vars.name.substring(0, Vars.name.length() - 1);
			else
				Vars.name += c;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mouseDragged(int, int, int, int)
	 */
	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		if (buttonPressed == 2 && oldy >= 276 && oldy <= 298 && oldx >= 450 && oldx <= 780) {
			settings[0] = (int) ((newx - 450) / 3.3);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mouseReleased(int, int, int)
	 */
	@Override
	public void mouseReleased(int button, int x, int y) {
		// If mouse is over the main buttons column
		if (buttonPressed == 0 && button == 0) {
			if (x >= 204 && x <= 404) {
				if (y >= 142 && y <= 200)
					buttonPressed = 1;
				else if (y >= 242 && y <= 300)
					buttonPressed = 2;
				else if (y >= 342 && y <= 400)
					buttonPressed = 3;
			} else if (x >= 604 && x <= 804) {
				if (y >= 142 && y <= 200)
					buttonPressed = 4;
				else if (y >= 242 && y <= 300)
					buttonPressed = 5;
				else if (y >= 342 && y <= 400)
					buttonPressed = 6;
			}
		}
		// Quit Menu
		else if (buttonPressed == 6 && button == 0 && y > 278 && y < 317) {
			if (x > 365 && x < 446) // Mouse over Yes
				buttonPressed = 7;
			else if (x > 565 && x < 628) // Mouse over No
				buttonPressed = 0;
		}
		// Credits Menu
		else if (buttonPressed >= 2 && buttonPressed <= 5 && button == 0 && y > 585 && y < 620 && x > 70 && x < 178) {
			buttonPressed = 0;
		}
		// Settings Menu
		else if (buttonPressed == 2 && button == 0) {
			if (y > 330 && y < 360) { // Music I/O
				if (x > 446 && x < 560)
					settings[1] = 0;
				else if (x > 558 && x < 708)
					settings[1] = 1;

			} else if (y > 390 && y < 420) { // Difficulty
				if (x > 446 && x < 546)
					settings[2] = 0;
				else if (x > 606 && x < 750)
					settings[2] = 1;
				else if (x > 809 && x < 909)
					settings[2] = 2;

			} else if (y >= 276 && y <= 298 && x >= 450 && x <= 780) {
				settings[0] = (int) ((x - 450) / 3.3);
			} else if (x >= 446 && x <= 566 && y >= 450 && y <= 480) {
				for (String[] row : Vars.scores)
					Arrays.fill(row, " ");
			} else if (x >= 446 && x <= 646 && y >= 490 && y <= 520) {
				setName = true;
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	@Override
	public int getID() {
		return MainMenu.ID;
	}
}
