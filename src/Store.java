
/*
	MIT License
	
	Copyright (c) 2018 Quantum Feather
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;

/**
 * The Class Store.
 */
public class Store extends BasicGameState {

	/** The Constant ID. */
	// ID we return to class 'Application'
	public static final int ID = Application.STORE;

	/** The food images. */
	Image[] food;

	/** The names of the foods. */
	String[] names;

	/** The descriptions of the foods. */
	String[] descriptions;

	/** The cost of the foods. */
	int[] cost;

	/** The food the mouse is currently hovering over. */
	int[] hoverFood;

	/** The current servings of fruits. */
	double[] fruits;

	/** The current servings of grains. */
	double[] grains;

	/** The current servings of dairy. */
	double[] dairy;

	/** The current servings of meat. */
	double[] meat;

	/** The current amount of fat. */
	int[] fat;

	/** The current amount of sodium. */
	int[] sodium;

	/** The current amount of potassium. */
	int[] potassium;

	/** The current amount of protein. */
	int[] protein;

	/** The current number of calories. */
	int[] cals;

	/** The menu choice. */
	int menuChoice;

	/** The selected food. */
	int selectedFood;

	/** The title font. */
	AngelCodeFont titleFont;

	/** The main font. */
	AngelCodeFont mainFont;

	/** The subtitle font. */
	AngelCodeFont subtitleFont;

	/** The text font. */
	AngelCodeFont textFont;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		food = new Image[25];
		names = new String[25];
		descriptions = new String[25];
		cost = new int[25];
		selectedFood = 0;
		hoverFood = new int[2];
		fruits = new double[25];
		grains = new double[25];
		dairy = new double[25];
		meat = new double[25];
		fat = new int[25];
		potassium = new int[25];
		sodium = new int[25];
		protein = new int[25];
		cals = new int[25];

		menuChoice = 0;
		selectedFood = 0;

		//reset blob stats
		Vars.blobFruits = 0;
		Vars.blobGrains = 0;
		Vars.blobDairy = 0;
		Vars.blobMeat = 0;

		Vars.blobSodium = 0;
		Vars.blobPotassium = 0;
		Vars.blobProtein = 0;
		Vars.blobFat = 0;

		// Load Food
		for (int x = 0; x < 25; x++) {
			food[x] = new Image(String.format("assets/food/tile%03d.png", x), false, Image.FILTER_NEAREST);
		}

		try (BufferedReader br = new BufferedReader(new FileReader("assets/food/food.csv"))) {
			int x = 0;
			br.readLine();
			for (String line; (line = br.readLine()) != null;) {
				for (int y = 0; y < 12; y++) {
					if (y == 0)
						names[x] = line.substring(0, line.indexOf(","));
					else if (y == 1) {
						descriptions[x] = line.substring(0, line.indexOf(","));
						// Use '^' in place of commas. This switches it back
						descriptions[x] = descriptions[x].replace('^', ',');
					} else if (y == 2)
						// If there's an error, you put a comma in the CSV, stupid.
						// Read the above comment 8 lines up
						cost[x] = Integer.parseInt(line.substring(0, line.indexOf(",")));
					else if (y == 3)
						fat[x] = Integer.parseInt(line.substring(0, line.indexOf(",")));
					else if (y == 4)
						sodium[x] = Integer.parseInt(line.substring(0, line.indexOf(",")));
					else if (y == 5)
						potassium[x] = Integer.parseInt(line.substring(0, line.indexOf(",")));
					else if (y == 6)
						protein[x] = Integer.parseInt(line.substring(0, line.indexOf(",")));
					else if (y == 7)
						fruits[x] = Double.parseDouble(line.substring(0, line.indexOf(",")));
					else if (y == 8)
						grains[x] = Double.parseDouble(line.substring(0, line.indexOf(",")));
					else if (y == 9)
						dairy[x] = Double.parseDouble(line.substring(0, line.indexOf(",")));
					else if (y == 10)
						meat[x] = Double.parseDouble(line.substring(0, line.indexOf(",")));
					else if (y == 11)
						cals[x] = Integer.parseInt(line.substring(0, line.length()));
					line = line.substring(line.indexOf(",") + 1, line.length());
				}
				x++;
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}

		// titleFont = 96px
		titleFont = new AngelCodeFont("assets/fonts/titleFont.fnt", new Image("assets/fonts/titleFont.png"));
		// mainFont = 32px
		mainFont = new AngelCodeFont("assets/fonts/mainFont.fnt", new Image("assets/fonts/mainFont.png"));
		// subtitleFont = 48px
		subtitleFont = new AngelCodeFont("assets/fonts/subtitleFont.fnt", new Image("assets/fonts/subtitleFont.png"));
		// textFont = 18px
		textFont = new AngelCodeFont("assets/fonts/textFont.fnt", new Image("assets/fonts/textFont.png"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.setBackground(Color.white);
		int count = 0;
		for (int y = 15; y < 635; y += 127)
			for (int x = 365; x < 1000; x += 127) {
				if (x == hoverFood[0] && y == hoverFood[1])
					g.setColor(Color.magenta);
				else
					g.setColor(Color.cyan);
				Rectangle rect = new Rectangle(x, y, 112, 112);
				g.draw(rect);
				g.fill(rect);
				g.drawImage(food[count].getScaledCopy(3.5f), x, y);
				count++;
			}

		// Display total money available
		g.setColor(Color.black);
		g.setFont(textFont);
		g.drawString("Money: $", 10, 10);
		g.drawString(Integer.toString(Vars.money), 120, 10);

		// Display current food
		g.drawImage(food[selectedFood].getScaledCopy((float) 270 / 32), 10, 30);

		// Display food name
		g.setColor(Color.black);
		g.setFont(mainFont);
		g.drawString(names[selectedFood], 10, 320);

		g.setFont(textFont);

		// Descriptions
		String desc = descriptions[selectedFood];
		if (desc.length() > 24) { // If longer than one line
			int last = desc.substring(0, 25).lastIndexOf(" ") + 1;
			g.drawString(desc.substring(0, last), 12, 360);
			if (desc.length() > 45) { // If longer than two lines
				int last2 = last + desc.substring(last, 45).lastIndexOf(" ") + 1;
				g.drawString(desc.substring(last, last2), 12, 390);
				g.drawString(desc.substring(last2), 12, 420);
			} else // If only one line
				g.drawString(desc.substring(last), 12, 390);
		} else
			g.drawString(desc, 12, 410);

		// Display Protein
		g.setColor(Color.decode("#1f5b89"));
		g.drawString("Protein: " + String.valueOf(protein[selectedFood]), 160, 450);

		// Display Fat
		g.setColor(Color.decode("#1f5b89"));
		g.drawString("Fat: " + String.valueOf(fat[selectedFood]), 160, 480);

		// Display Potassium
		g.setColor(Color.decode("#1f5b89"));
		g.drawString("Potassium: " + String.valueOf(potassium[selectedFood]), 160, 510);

		// Display Sodium
		g.setColor(Color.decode("#1f5b89"));
		g.drawString("Sodium: " + String.valueOf(sodium[selectedFood]), 160, 540);

		DecimalFormat format = new DecimalFormat("0.##");

		// Display Fruits
		g.setColor(Color.decode("#223b05"));
		g.drawString("Fruits: " + format.format(fruits[selectedFood]), 12, 450);

		// Display Grains
		g.setColor(Color.decode("#223b05"));
		g.drawString("Grains: " + format.format(grains[selectedFood]), 12, 480);

		// Display Dairy
		g.setColor(Color.decode("#223b05"));
		g.drawString("Dairy: " + format.format(dairy[selectedFood]), 12, 510);

		// Display Meat
		g.setColor(Color.decode("#223b05"));
		g.drawString("Meat: " + format.format(meat[selectedFood]), 12, 540);

		// Display Calories
		g.setColor(Color.magenta);
		g.drawString("Calories: " + String.valueOf(cals[selectedFood]), 160, 570);

		// Display Cost
		g.setColor(Color.orange);
		g.drawString("Cost: " + String.valueOf(cost[selectedFood]), 12, 570);

		// Buy button
		g.setFont(mainFont);
		g.setColor(Color.cyan);
		g.drawString("Buy", 14, 598);
		g.drawRect(10, 600, 90, 30);

		// Back button
		g.setFont(mainFont);
		g.setColor(Color.pink);
		g.drawString("Back", 244, 598);
		g.drawRect(240, 600, 110, 30);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer,
	 * org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int arg2) throws SlickException {
		if (Vars.currentLevel == 3) {
			Vars.currentLevel++;
			System.out.println("asduaiusdhauisdhaiusgdaiusdg");
			sbg.getState(Application.ACT2).init(gc, sbg);
			sbg.enterState(Application.ACT2, new EmptyTransition(), new EmptyTransition());
		}
		if (Vars.currentLevel == 7) {
			Vars.currentLevel++;
			sbg.getState(Application.ACT3).init(gc, sbg);
			sbg.enterState(Application.ACT3, new EmptyTransition(), new EmptyTransition());
		}
		if (Vars.currentLevel == 11) {
			Vars.currentLevel++;
			sbg.getState(Application.ACT4).init(gc, sbg);
			sbg.enterState(Application.ACT4, new EmptyTransition(), new EmptyTransition());
		}
		if (menuChoice == 1) {

			System.out.println(Vars.currentLevel / 4 + 3);
			sbg.getState(Vars.currentLevel / 4 + 3).init(gc, sbg);
			sbg.enterState(Vars.currentLevel / 4 + 3, new EmptyTransition(), new EmptyTransition());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mouseMoved(int, int, int, int)
	 */
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		if ((newx - 365) % 127 <= 112 && (newy - 15) % 127 <= 112 && newx >= 365 && newy >= 17) {
			hoverFood[0] = newx - (newx - 365) % 127;
			hoverFood[1] = newy - (newy - 15) % 127;
		} else
			Arrays.fill(hoverFood, 0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#mousePressed(int, int, int)
	 */
	@Override
	public void mousePressed(int button, int x, int y) {
		if (button == 0) {
			if (x >= 10 && x <= 100 && y >= 600 && y <= 630) {
				if (cost[selectedFood] <= Vars.money) {
					Vars.money -= cost[selectedFood];
					Vars.blobFruits += fruits[selectedFood];
					Vars.blobGrains += grains[selectedFood];
					Vars.blobDairy += dairy[selectedFood];
					Vars.blobMeat += meat[selectedFood];
					Vars.blobProtein += protein[selectedFood];
					Vars.blobPotassium += potassium[selectedFood];
					Vars.blobFat += fat[selectedFood];
					Vars.blobSodium += sodium[selectedFood];
					System.out.println(Vars.blobGrains);
				}
			} else if (x >= 240 && x <= 350 && y >= 600 && y <= 630) {
				menuChoice = 1;
			} else if ((x - 365) % 127 <= 112 && (y - 15) % 127 <= 112 && x >= 365 && x < 986 && y >= 17) {
				// Select Food
				selectedFood = (x - 365) / 127;
				selectedFood += (y - 15) / 127 * 5;
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	@Override
	public int getID() {
		return Store.ID;
	}
}
