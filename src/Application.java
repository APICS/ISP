
/*
	MIT License
	
	Copyright (c) 2018 Quantum Feather
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * The driver class for the entire class. Contains ID numbers, version #, window
 * width & height, and FPS.
 */
public class Application extends StateBasedGame {

	/** The Constant BLACKSCREEN. */
	public static final int BLACKSCREEN = 0;

	/** The Constant SPLASHSCREEN. */
	public static final int SPLASHSCREEN = 1;

	/** The Constant MAINMENU. */
	public static final int MAINMENU = 2;

	/** The Constant LEVEL1. */
	public static final int LEVEL1 = 3;

	/** The Constant LEVEL2. */
	public static final int LEVEL2 = 4;

	/** The Constant LEVEL3. */
	public static final int LEVEL3 = 5;

	/** The Constant STORE. */
	public static final int STORE = 6;

	/** The Constant VARS. */
	public static final int VARS = 99;

	/** The Constant ACT1. */
	public static final int ACT1 = 7;

	/** The Constant ACT2. */
	public static final int ACT2 = 8;

	/** The Constant ACT3. */
	public static final int ACT3 = 9;

	/** The Constant ACT4. */
	public static final int ACT4 = 10;

	/** The Constant WIDTH. */
	public static final int WIDTH = 1000;

	/** The Constant HEIGHT. */
	public static final int HEIGHT = 650;

	/** The Constant FPS. */
	public static final int FPS = 60;

	/** The Constant VERSION. */
	public static final double VERSION = 1.0;

	/**
	 * Instantiates a new application.
	 *
	 * @param appName
	 *            the app name
	 */
	public Application(String appName) {
		super(appName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.newdawn.slick.state.StateBasedGame#initStatesList(org.newdawn.slick.
	 * GameContainer)
	 */
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new BlackScreen());
		this.addState(new Vars());

		this.addState(new SplashScreen());
		this.addState(new MainMenu());
		this.addState(new Store());

		this.addState(new Act1());
		this.addState(new Act2());
		this.addState(new Act3());
		this.addState(new Act4());
		this.addState(new SplashScreen());
		this.addState(new MainMenu());
		this.addState(new Store());

		this.addState(new Level1());
		this.addState(new Level2());
		this.addState(new Level3());
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		try {
			AppGameContainer app = new AppGameContainer(new Application("Fisticuffs"));
			app.setDisplayMode(WIDTH, HEIGHT, false);
			String[] icons = { "assets/icons/32x32.png", "assets/icons/16x16.png" };
			app.setIcons(icons);
			app.setTargetFrameRate(FPS);
			app.setShowFPS(false);
			app.start();
		} catch (SlickException e) {
		}
	}
}