Tyler
Congratulations! You beat your second arena!
You
That wasn't that bad.
Tyler
Well, now that you know how to correctly balance food groups & nutrients we can go to the next level.
You
There's more?
Tyler
Of course!
Tyler
Now we go onto becoming a big strong man!
You
What, I already ate healthy and balanced my food groups and nutrients.
Tyler
Well now we need you to gain a lot more muscle, so you need more protein.
You
How much more?
Tyler
Well you're around 170 lb right?
You
Yeah...
Tyler
For gaining muscle it is recommended to have 0.9g/lb of body weight of protein.
You
Well that comes to around 150 g!
Tyler
You're catching on!
Tyler
Remember, protein is 150 g now and all the other nutrient values are the same.
You
I kinda forgot how much i need.
Tyler
2300 mg of sodium.
Tyler
4700 mg of potassium.
Tyler
100 g of fat.
Tyler
And now 150 g of protein.
You
Alright, and food groups?
Tyler
You really need to pay more attention.
Tyler
8 Fruits & Veggies, 7 Grain, 3-4 Dairy, 3 Meat.
You
Alright I think i got it.
Tyler
This is the last arena, so after this you can be free!
You
Yay!
