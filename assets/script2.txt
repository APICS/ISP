Tyler
Congratulations! You beat your first arena!
You
That wasn't that bad.
Tyler
Well, now that you know how to correctly balance food groups we can go to the next level.
You
There's more?
Tyler
Of course! Didn't you notice the nutrient values in the shop?
You
Well, I didn't think they were important...
Tyler
They are very important.
Tyler
There are 4 Big nutrients we need.
Tyler
Sodium, Potassium, Fat, and Protein.
You
How much?
Tyler
Well for a regular 16 year old should have 2300 mg of sodium.
Tyler
4700 mg of potassium.
Tyler
100 g of fat.
Tyler
52 g of protein.
You
Wow, thats a lot of protein.
Tyler
Well yeah, you need a lot as a teenage boy.
Tyler
Anyways, thats it for now, just know that your strength and health will be affected!
Tyler
Smell ya later
You
...
