# Quantum Feather - Weekly Plan
**Week of:** May 22<br>
**Project:** Fisticuffs

## Alim Zaib
### Tasks for the week
* Infinite Background scrolling
* Multiple Enemies
* Improve AI
* Script Writing


## Kyle Schwartz
### Tasks for the week
* Finish Main Menu
* Setup Shop
    * Add Items
    * Set prices
    * Add descriptions