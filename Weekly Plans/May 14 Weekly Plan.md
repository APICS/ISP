# Quantum Feather - Weekly Plan
**Week of:** May 14<br>
**Project:** Fisticuffs

## Alim Zaib
### Tasks for the week
* Figure out how to use spritesheets with Slick2D
* Find spritesheets for the characters in the game
* Create the Main Menu
* Create the credits and exit buttons


## Kyle Schwartz
### Tasks for the week
* Setting up Slick2D
* Getting up GitLab
* Get Collisions working
* Setup Shop