# Quantum Feather - Weekly Plan
**Week of:** May 28<br>
**Project:** Fisticuffs

## Alim Zaib
### Tasks for the week
* Implement Levels
* Script Writing
* Health Bars
* Different Enemies


## Kyle Schwartz
### Tasks for the week
* Add Shop Data
* Script Writing
* Dialog Boxes