# Quantum Feather - Weekly Plan
**Week of:** June 4<br>
**Project:** Fisticuffs

## Alim Zaib
### Tasks for the week
* Fix hitboxes
* Finalise script
* Quality Control
* Choreograph cut scenes


## Kyle Schwartz
### Tasks for the week
* Implement script into game
* Finalise store prices
* Quality Control
* Merging Alim's big code