# <u>Summary of Work Logs</u>

## Kyle Schwartz - 26 Hours, 40 Minutes
### Tasks Worked On
* Decided on engine
* Cleaned Up & Finalized Storyboard
* Setup Engine
* Implemented Splash-screen
* Setup GitLab
* Setup GitLab branches
* Added Credits menu
* Added Quit Menu
* Added music toggle, volume slider, difficulty option
* Setup shop with:
	* food names & pictures
	* money
	* buy button
	* currently selected food preview
* Switched to CSV for store data
* Added dialog boxed with multiple lines
* Made dialog boxes easier to read
* Animated dialog

### Challenges
* Git merge caused errors
* Unable to read file within JAR
* Indecisive on which engine to use
* Working on multiple git branches was tricky
* Difficulty parsing CSV file
* Unable to create JAR at school

### New Technical Skills
* Proficient in Slick2D
* Understand how to maintain a git work-flow
* Discovered the string method lastIndexOf()
* Reading from a CSV file
* How to Javadoc properly
* How to convert OTF to AngelCodeFont

<br><br>

### New Knowledge of Self
* Really good at troubleshooting
* Quick learner
* No problems getting along with people
* Very Supportive

### Distribution of Work Was Equal
Name: Kyle Schwartz <br>
Date: ____________________ <br>
Signature: ____________________

<br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br>
<br><br><br><br><br>

## Alim Zaib - 28 Hours, 30 Minutes
### Tasks Worked On
* Started Storyboard
* Implemented sprite sheets
* Implemented Character:
	* Movement
	* Hitboxes
	* Animation
	* Health
	* Damage
	* Scaling
	* Attacks
* Created 3 game levels
* Implemented nutritional values into the game
* Implemented win / lose screens
* Animated enemy deaths
* Implemented punch sound effect

### Challenges
* Git merging had numerous errors
* Creating multiple enemies
* KISSing code to reduce lag
* Difficulty creating an infinite length arena
* Smooth animation

### New Technical Skills
* Learned how to use Git
* Learned how to create and use sprite sheets
* Gained proficiency in Slick2D
* Learned how to create hitboxes

### New Knowledge of Self
* Can get stuff done when it needs to be
* Can reliably give a time estimate of how long something will take
* Can get along with others

### Distribution of Work Was Equal
Name: Alim Zaib <br>
Date: ____________________ <br>
Signature: ____________________
