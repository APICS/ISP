# <u>Self Evaluation - Alim</u>

I think that I did a good job on this project, but I could’ve done much better. 
I readily met deadlines but quality suffered a little because of it. I think 
that if I just put a little more time into the project it could’ve been better. 
I also think that I should’ve utilized GitLab’s offered features like issues 
and tasks a little more effectively so the project would’ve been a little more 
organized. I also would’ve made the code I wrote a lot more broken up into 
organized pieces. Currently I just made the entities in the level class and 
where the enemies are made, hitboxes are detected, speeds are determined, etc. 
are all in the same spot. There are sections of code that do completely different 
things right next to each other, and I feel like I could’ve made some more 
methods to make my own life a little better.
