# <u>Technical Manual</u>

## Main Menu
<img src="https://image.ibb.co/cSMX2o/2018_06_10_21_06_20.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
On the main menu, you can choose one of six options; Play, Settings, Rules, Scores, Credits, Quit.

### Play
This will bring you to the main game. See below.

### Settings
<img src="https://preview.ibb.co/h2G4F8/2018_06_10_21_15_37.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
The settings menu allows you to change the following settings:

* Volume
	* Slide within the box or click to set the volume percentage.
* Music
	* Click either the ```ON``` or ```OFF``` switch to set the music accordingly.
* Difficulty
	* Click any of the three options to set the difficulty accordingly.
* High-scores
	* Click the ```CLEAR``` button to clear all of the high scores.
	* Click the ```SET NAME``` button to set your name. 
	  Type any name you desire, excluding profanity, and press ```ENTER``` when complete.

Press the ```BACK``` button to go back.
	
### Rules
<img src="https://preview.ibb.co/fQFbdT/2018_06_10_21_46_54.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
The rules menu explains the game controls. Press the ```BACK``` button to go back.

### Scores
<img src="https://image.ibb.co/iRXxJT/2018_06_10_21_47_57.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
The scores menu allows you to view the high scores. Press the ```BACK``` button to go back.

### Credits
<img src="https://image.ibb.co/hkgrdT/2018_06_10_21_48_06.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
The credits menu allows you to view the credits. Press the ```BACK``` button to go back.

<br><br><br><br><br>

### Quit
<img src="https://image.ibb.co/goeYso/2018_06_10_21_48_16.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
The quit menu offers you the option to quit the game. Pressing ```YES``` will close the game immediately. 
Pressing ```NO``` will bring you back to the main menu.

## Main Game

### Cutscenes
<img src="https://image.ibb.co/hTcHk8/2018_06_10_21_58_20.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
These explain the story and build out the world. Press any key to continue.

### Shop
<img src="https://image.ibb.co/eq2iQ8/2018_06_10_21_58_52.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
Here you are able to buy items to improve your stats. Left click any of the 25 foods to preview them. 
Press the ```BUY``` button to purchase the selected food. 
Press the ```BACK``` button once you have completed all of your transactions.

<br><br><br>

### Game
<img src="https://image.ibb.co/b3aRdT/2018_06_10_21_59_08.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
As explained in the rules section:

* **X**: Punch
* **Left Arrow**: Move left
* **Right Arrow**: Move right
* **Up Arrow**: Jump

### Unconscious
<img src="https://image.ibb.co/mETOQ8/2018_06_10_21_59_19.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
Press ```YES``` to restart the level, or ```NO``` to return to the main menu.

### Beat Level
<img src="https://image.ibb.co/k6o1Co/2018_06_10_21_59_32.png" style="float: left;width:350px;margin: 0px 10px 0px 0px"/>
Press ```YES``` to continue to the next level, or ```NO``` to return to the main menu.
