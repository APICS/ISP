# <u>Biggest Success</u>

## Kyle
The biggest success I had was the creation of the dialog boxes. It 
was a very daunting task as I was unsure as to how to break up the lines
without cutting words in half. This is when I learnt about the lastIndexOf()
method. This made it very easy to break the lines up correctly. But when it
came to the animation, I'm still not sure how I did it. Over the span of roughly
30 minutes I wrote the entire code and since then I have not been able to 
understand it given how many different string methods are working in tandem
to somehow make the animation magically work. I am nonetheless proud of it.

## Alim
The biggest success I had was getting hit boxes to work on the enemy and player. 
Due to some libraries that Slick2D provides, hit box detection was fairly simple. 
Afterwards I simply added an immunity timer so each enemy and the player would be 
invulnerable for a bit and it worked seamlessly.
