# <u>Self Evaluation - Kyle</u>

I feel that I have this project most, but not all of what I had to offer.
I was very enthusiastic throughout the past month as I truly enjoyed making
this. But, there were areas where I cut corners and should have done a better
job. This mostly came in the organizational aspect where everything was simply
written in the init(), render(), and update() methods instead of making methods
for each thing being drawn and then calling them within the 3 aforementioned
methods. I did however do some things that I was quite proud of such as the
shop. Aspects such as the color changing when hovered over and pixel perfect
mouse detection were some of my favourite things to work on. I am also impressed
with myself that I used a CSV to store most of the data as doing it in the
source code would have been terrible style and using a text file wouldn't
have been good for editing the data. Overall though, I feel as though I did 
a really good job and am very proud of the final result and how I acted the 
whole way through.