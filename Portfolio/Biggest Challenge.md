# <u>Biggest Challenges</u>

## Kyle
The biggest challenge I faced came at the very end of the project.
Whilst developing the game, file reading worked perfectly. But once
it was packaged up into a runnable JAR file, it refused to read from
text files within the JAR itself. It took several hours to figure out
the source of the crashing and several more hours to find a solution.
The only solution that worked was placing the text files in a folder
adjacent to the jar itself and then get the path of the text files
by finding the absolute path of the parent directory of the class file
being called then then tacking on the relative directory of the text
file.

## Alim
The biggest challenge I faced when making the game was making an 
arena of infinite length. While making arena of infinite length 
it involves bounding boxes on the sides that won’t let the character 
go past but rather it will move the background. The problem was that 
the enemy’s would also have to behave as if the character was moving
 normally in the middle of the screen and getting relative speeds between 
 the character, enemies and background to work cohesively was a big 
 challenge. I overcame it through basically running every test case 
 possible until I got the numbers right.
