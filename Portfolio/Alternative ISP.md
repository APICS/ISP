# <u>Alternative ISPs</u>

## Kyle
If I could chose the ISP, I would have preferred it to be more like the grade
10 & 11 ISP. I liked the idea of recreating classic games to the best of our
abilities. Instead of being given a predetermined game, if we were able to
chose from a list, that would have been a nice change for the grade 12s.
From my personal experience, 2 people can look at two different assignments
and disagree on which one is easy and which one is hard. Everyone has a
different way of tackling a problem and that's what would make the choosing
from a list much better than being given one. Of course these would have to
be much harder than the grade 10 & 11 ISPs, but I still think it could work.

## Alim
I would’ve wanted the ISP to be about teaching people some sort of skill. 
For example a game could be made to teach people how to drive and include 
things like switching gears or how to drive manual cars. Or something like 
how to cook and have recipes and make the user cook a few things. This way 
some useful knowledge can be gained in an interactive way without actually 
having to do the activity.