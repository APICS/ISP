# <u>Regrets</u>

## Kyle
The only regret I have is not making the game dynamically scalable. If we
had significantly more time and a bit more foresight, I could've made every
variable dynamic and based off of a handful of constants such as the screen
size. This would free us from the constraints that we set ourselves to such
as the window being 1000px * 650px. The user would be able to scale the
window to whatever size they pleased & we wouldn't have to do pixel measurements
to make sure everything was centered and in the right place.

## Alim
I regret not making entity classes. Each entity I made was stored in 
the level class where it was used. If I simply used an object-oriented 
approach and had an enemy class, and a character class It would’ve made 
my life a lot easier and would have made the game code much easier to read. 
For the future I will make sure to future proof myself so if I have to 
change the code at a later date I make my own life easier.
