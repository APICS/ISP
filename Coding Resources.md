# Coding Resources

## Fonts

* [Creating New Fonts](https://stackoverflow.com/a/10263479)

## Markdown

* [MD to PDF](http://www.markdowntopdf.com/)

## Final Application

* [Jarsplice](http://ninjacave.com/resources/jarsplice/jarsplice-0.40.jar)
* [How To Use Jarsplice](https://www.youtube.com/watch?v=K6K2d6LGvu8)
* TL;DW:
    1. Export as non runnable jar
    1. Add jar from previous step & jars from `lib/slick/lib` (jinput, jogg, jorbis, lwjgl, lwjgl_util, slick)
    1. Add natives from `/lib/lwjgl-2.9.3/native/windows` (all files)
    1. Main class: `Application`
    1. Create Fat Jar

## Music
* [Dank 8-Bit](https://ozzed.bandcamp.com/)