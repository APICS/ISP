# Quantum Feather - Time Sheet
**Week of:** June 8th<br>
**Project:** Fisticuffs

## Kyle Schwartz

### Monday
* 1 Hour, 20 mins
* Fix file reading bug

### Tuesday
* 1 Hour, 20 mins
* Fix user input breaking bug

### Wednesday
* 1 Hour, 20 mins
* Balance game around numbers

### Thursday
* 1 Hour, 20 mins
* Javadoc
* Burn disk
* Make cover

### Friday
* 1 Hour, 20 mins
* Create 2nd Disk with better scaling

## Alim Zaib

### Monday
* 1 Hour, 20 mins
* Create scaling based on food group and nutrient values

### Tuesday
* 1 Hour, 20 mins
* Finish script and implementation

### Wednesday
* 1 Hour, 20 mins
* Clean code and make more KISS

### Thursday
* 1 Hour, 20 mins
* Javadoc

### Friday
* 1 Hour, 20 mins
* Create 2nd Disk with better scaling