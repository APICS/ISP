# Quantum Feather - Time Sheet
**Week of:** May 28nd<br>
**Project:** Fisticuffs

## Kyle Schwartz

### Monday
* 1 Hour, 20 mins
* Dialog Boxes
 * Coded multiline scripts
 * Learnt about lastIndexOf()
* Fixed Buy Button

### Tuesday
* 1 Hour, 20 mins
* Merged code with master
* Learnt about cherry picking'
* Researched git branches further


### Wednesday
* 1 Hour, 20 mins
* Added white box around dialog for easier reading
* Learnt about filling boxes
* Added character-by-character animation

### Thursday
* 1 Hour, 20 mins
* Added Music
* Fixed volume slider
* Made music On/Off work
        
### Friday
* 1 Hour, 20 mins
* Resolving git issues
* Bug hunting


## Alim Zaib

### Monday
* 1 Hour, 20 mins
* Started writing script

### Tuesday
* 1 Hour, 20 mins
* Finished writing script
* Fixed kill counter

### Wednesday
* 1 Hour, 20 mins
* Added health indicator
* Added death animation

### Thursday
* 1 Hour, 20 mins
* Added half of shop integration

### Friday
* 1 Hour, 20 mins
* Added punch sound effects
