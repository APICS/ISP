# Quantum Feather - Time Sheet
**Week of:** May 14th<br>
**Project:** Fisticuffs

## Kyle Schwartz

### Monday
* 1 Hour, 20 mins
* Got engine files
* Setup engine
* Troubleshooted engine
* Downloaded older engine version
* Got engine to work

### Tuesday
* 0 Hours
* AP Test

### Wednesday
* 1 Hour, 20 mins
* Began git integration
* Implemented splashscreen
    * Fade in
    * Fade Out
    * Logo
    * Company Name

### Thursday
* 1 Hour, 20 mins
* Finished git configuration
    * Setup multiple brances
    * Setup workflow for merge requests
* Began main menu
    * Setup credits menu
    * Setup quit menu
        

### Friday
* 1 Hour, 20 mins
* Began settings menu
    * Added music toggle
    * Added volume slider
    * Added dificulty option

## Alim Zaib

### Monday
* 1 Hour, 20 mins
* Got sprites to work in Slick2d

### Tuesday
* 0 Hours
* AP Exam

### Wednesday
* 1 Hour, 20 mins
* Created Skeleton code for spritesheet implementation

### Thursday
* 1 Hour, 20 mins
* Made a walking animation and took user input to move the character

### Friday
* 1 Hour, 20 mins
* Idle and punch animations to work
