# Quantum Feather - Time Sheet
**Week of:** May 22nd<br>
**Project:** Fisticuffs

## Kyle Schwartz

### Monday
* 0 Hours
* Holiday

### Tuesday
* 1 Hour, 20 mins
* Began shop creation
    * Added fruit names
    * Added fruit pictures
    * Basic collision

### Wednesday
* Completed shop
    * Perfect hitbox detetion
    * Implemented csv file for array storage
    * Implemented buy button + money
    * Added currently selected picture

### Thursday
* 0 Hours
* Under the weather
        

### Friday
* 0 Hours
* Under the weather

## Alim Zaib

### Monday
* 0 Hours

### Tuesday
* 1 Hour, 20 mins
* Finished enemy AI
* Started working on Infinite Scrolling

### Wednesday
* 1 Hour, 20 mins
* Finished infinite scrolling
* Started working on creating multiple enemy copies

### Thursday
* 1 Hour, 20 mins
* Finished multiple enemy copies
* Finished win & death screens

### Friday
* 30 mins
* Started working on parralax effect