# Quantum Feather - Time Sheet
**Week of:** May 7th<br>
**Project:** Fisticuffs

## Kyle Schwartz

### Monday
* 1 Hour, 20 mins
* Researched several game engines
* Decided on Litiengine

### Tuesday
* 1 Hour, 20 mins
* Found Litiengine documentation to be too poor
* Switched to Slick2D

### Wednesday
* 1 Hour, 20 mins
* Began Storyboarding ideas
* Worked on Menu Screens

### Thursday
* 1 Hour, 20 mins
* Cleaned up storyboard
* Finalised storyboard

### Friday
* 0 Hours
* AP Practise Test - No Work Completed

## Alim Zaib

### Monday
* 1 Hour, 20 mins
* Brainstormed ideas for our game
* Decided focus would be on healthy eating

### Tuesday
* 1 Hour, 20 mins
* Decided a top-down beat-em-up style game would be fun
* Started looking for copyright free game assets

### Wednesday
* 1 Hour, 20 mins
* Started storyboard
* Due to lack of assets, top-down was switched to side scrolling
* worked on menu screens on storyboard

### Thursday
* 1 Hour, 20 mins
* Worked on storyboard and completed it

### Friday
* 0 Hours
* No work was done